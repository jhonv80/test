﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using InsuranceDB;

namespace WebMVC.Controllers
{
    public class PoliciesController : Controller
    {
        private InsuranceTestEntities db = new InsuranceTestEntities();

        // GET: Policies
        public ActionResult Index()
        {
            var policies = db.Policies.Include(p => p.Coverage).Include(p => p.Customer).Include(p => p.PolicyState).Include(p => p.RiskType);
            return View(policies.ToList());
        }

        // GET: Policies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Policy policy = db.Policies.Find(id);
            if (policy == null)
            {
                return HttpNotFound();
            }
            return View(policy);
        }

        // GET: Policies/Create
        public ActionResult Create()
        {
            ViewBag.IdCoverage = new SelectList(db.Coverages, "Id", "Name");
            ViewBag.IdCustomer = new SelectList(db.Customers, "Id", "FirstName");
            ViewBag.IdState = new SelectList(db.PolicyStates, "Id", "Name");
            ViewBag.IdRiskType = new SelectList(db.RiskTypes, "Id", "Name");
            return View();
        }

        // POST: Policies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Description,IdCoverage,StartPolicy,IdRiskType,IdCustomer,IdState")] Policy policy)
        {
            if (policy.IdRiskType ==4)
            {
                var cov = db.Coverages.Where(x => x.Id == policy.IdCoverage).FirstOrDefault();
                if (cov.percentage_____Percentage > 50)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }

            if (ModelState.IsValid)
            {
                db.Policies.Add(policy);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.IdCoverage = new SelectList(db.Coverages, "Id", "Name", policy.IdCoverage);
            ViewBag.IdCustomer = new SelectList(db.Customers, "Id", "FirstName", policy.IdCustomer);
            ViewBag.IdState = new SelectList(db.PolicyStates, "Id", "Name", policy.IdState);
            ViewBag.IdRiskType = new SelectList(db.RiskTypes, "Id", "Name", policy.IdRiskType);

            return View(policy);
        }

        // GET: Policies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Policy policy = db.Policies.Find(id);
            if (policy == null)
            {
                return HttpNotFound();
            }
            ViewBag.IdCoverage = new SelectList(db.Coverages, "Id", "Name", policy.IdCoverage);
            ViewBag.IdCustomer = new SelectList(db.Customers, "Id", "FirstName", policy.IdCustomer);
            ViewBag.IdState = new SelectList(db.PolicyStates, "Id", "Name", policy.IdState);
            ViewBag.IdRiskType = new SelectList(db.RiskTypes, "Id", "Name", policy.IdRiskType);
            return View(policy);
        }

        // POST: Policies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Description,IdCoverage,StartPolicy,IdRiskType,IdCustomer,IdState")] Policy policy)
        {
            if (ModelState.IsValid)
            {
                db.Entry(policy).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.IdCoverage = new SelectList(db.Coverages, "Id", "Name", policy.IdCoverage);
            ViewBag.IdCustomer = new SelectList(db.Customers, "Id", "FirstName", policy.IdCustomer);
            ViewBag.IdState = new SelectList(db.PolicyStates, "Id", "Name", policy.IdState);
            ViewBag.IdRiskType = new SelectList(db.RiskTypes, "Id", "Name", policy.IdRiskType);
            return View(policy);
        }

        // GET: Policies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Policy policy = db.Policies.Find(id);
            if (policy == null)
            {
                return HttpNotFound();
            }
            return View(policy);
        }

        // POST: Policies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Policy policy = db.Policies.Find(id);
            db.Policies.Remove(policy);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
